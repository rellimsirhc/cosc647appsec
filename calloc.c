#include <stdio.h>
#include <stdlib.h>

int main () { 
 
 float *nums; int N, i;
 printf("Read how many numbers:");
 scanf("%d", &N);
 
 nums = (float *) calloc(N, sizeof(float));
 
 if (nums == NULL) {
 	printf("ERROR");
 	exit (0);}

/* nums is now an array of floats of size N*/
for(i=0; i < N ;i++) {
printf("Please enter number %d: ", i+1);
scanf("%f", &(nums[i]));
}

printf("nums= %8x\n"; nums);

free(nums);nums=NULL;

printf("nums= %8x\n"; nums);

free(nums);
 return 0;
}


