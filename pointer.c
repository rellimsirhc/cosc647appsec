#include <stdio.h>


int main() {
int int1 = 1036;
int int2 = 8;
int *ptr1 = &int1;
int *ptr2 = &int2;

ptr1 = *ptr2;
ptr2 = ptr1;

printf("ptr1=%8x\n", ptr1);
printf("ptr2=%8x\n", ptr2);
printf("\*ptr2=%8x\n", *ptr2);
return 0;
}
